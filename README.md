# Pet Adoption App - Flutter Task

This is flutter app for a take-home task.

## Feature checklist
- [X] List of pet animals with photo thumbnail.

- [X] Details page with image & adopt me button
    - [X] Image Zoom (Using Flutter's built-in InteractiveViewer)
    - [X] Confetti on pressing adopt
    - [X] Hero animation on navigation to details page

- [X] Search animals (substring of name)
    - [X] Additional filters: Select one or more categories through checkbox

- [X] Used BLoC pattern

- [X] History page

- [X] Show adoption status on homepage list, and grey out the animal

- [X] Persist adoption status: Used shared preferences library

- [X] Unit tests

- [X] Widget tests

- [ ] Integration test

- [ ] Attractive UI: Not sure. 😂

- [ ] CLEAN code / SOLID: This is a small app developed in 2 days, and thus took some shortcuts. A global variable `List<Pet> pets` works as data source and all changes are written to it. Search query is also stored in a global for simplicity. History list is recomputed every time it needs to be rendered, which can probably be more efficient. Apart from this, I have tried to separate UI and Data.

- [ ] Pagination (extra feature): Probably will not be able to implement this.

## Other details

Platform used during development: Android

Photos of Animals were obtained from Pixabay.

To reset animal adopted state, clear app data. (☹️)