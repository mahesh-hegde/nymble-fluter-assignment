// Some simple unit tests
import 'package:pet_adoption_app_assignment/data.dart';
import 'package:test/test.dart';

List<Pet> searchPets(String query, Set<PetType> types) {
  return getPetIndices(Search.of(query, types)).map((i) => pets[i]).toList();
}

void main() {
  test("Test copying of pet details with adoption date", () {
    Pet cat = Pet(
        name: "Billa",
        type: PetType.cat,
        age: 16,
        price: 2000,
        imageAssetName: "n/a");
    final now = DateTime.now();
    Pet adoptedCat = Pet.withAdoptionDate(cat, now);
    expect(cat.name, adoptedCat.name);
    expect(cat.age, adoptedCat.age);
    expect(cat.price, adoptedCat.price);
    expect(cat.imageAssetName, adoptedCat.imageAssetName);
    expect(cat.adoptedOn, null);
    expect(adoptedCat.adoptedOn, now);

    expect(adoptedCat.isAdopted, true);
    expect(cat.isAdopted, false);
  });

  test("Test search by type functionality", () {
    expect(getPetIndices(Search.of("Dinga", {PetType.cat})), <int>[]);
    expect(
      searchPets("", {PetType.cat}),
      pets.where((pet) => pet.type == PetType.cat).toList(),
    );
    expect(
      searchPets("", PetType.values.toSet()),
      pets,
    );
  });
}
