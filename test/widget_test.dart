// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:pet_adoption_app_assignment/data.dart';

import 'package:pet_adoption_app_assignment/main.dart';

void main() {
  testWidgets('Test homescreen widget', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const PetsApp());

    // Verify that our counter starts at 0.
    for (final text in ["Pets", "History"]) {
      expect(find.text(text), findsOneWidget);
    }
    expect(find.text('Counter'), findsNothing);

    expect(find.byType(TextFormField), findsOneWidget);
    expect(find.byType(Checkbox), findsNWidgets(PetType.values.length));
  });

  testWidgets("Test Pet details page", (tester) async {
    await tester.pumpWidget(
      MaterialAppWrapper(
        BlocProvider<PetCubit>(
          create: (context) => PetCubit(0),
          // We can't load assets properly in a widget test window.
          // So do not load the image.
          child: const PetDetailsPage(showImage: false),
        ),
      ),
    );
    await tester.pumpAndSettle();
    expect(find.text("Name: ${pets[0].name}"), findsOneWidget);
  });
}

class MaterialAppWrapper extends StatelessWidget {
  const MaterialAppWrapper(this.child, {Key? key}) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: "LiveState Test", home: child);
  }
}
