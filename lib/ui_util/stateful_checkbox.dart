import 'package:flutter/material.dart';

// The checkbox in flutter does not change its visual state unless parent widget
// rebuilds it.
// This is a simple wrapper around it.

typedef CheckboxHandler = void Function(bool?);

class StatefulCheckbox extends StatefulWidget {
  const StatefulCheckbox({
    required this.label,
    required this.initialState,
    required this.onChange,
    Key? key,
  }) : super(key: key);
  final String label;
  final bool initialState;
  final CheckboxHandler onChange;

  @override
  State<StatefulWidget> createState() => _StatefulCheckboxState();
}

class _StatefulCheckboxState extends State<StatefulCheckbox> {
  bool state = false;
  @override
  void initState() {
    super.initState();
    state = widget.initialState;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        children: [
          Checkbox(
              activeColor: Colors.deepPurple,
              value: state,
              onChanged: (s) {
                widget.onChange(s);
                setState(() => state = !state);
              }),
          Text(widget.label),
        ],
      ),
    );
  }
}
