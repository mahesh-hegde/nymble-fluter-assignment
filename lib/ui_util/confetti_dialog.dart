import 'package:flutter/material.dart';
import 'package:confetti/confetti.dart';

Future<T?> showConfettiDialog<T>({
  required BuildContext context,
  required String text,
}) async {
  final pageBuilder = Center(
      child: Text(
    text,
    style: Theme.of(context).textTheme.headline2,
    textAlign: TextAlign.center,
  ));
  ConfettiController confettiController =
      ConfettiController(duration: const Duration(seconds: 2));
  confettiController.play();
  final result = await showDialog<T>(
    context: context,
    builder: (BuildContext buildContext) {
      return Stack(
        children: [
          pageBuilder,
          Align(
            alignment: Alignment.center,
            child: ConfettiWidget(
              confettiController: confettiController,
              blastDirectionality: BlastDirectionality.explosive,
              emissionFrequency: 0.1,
            ),
          ),
        ],
      );
    },
    barrierDismissible: true,
    barrierColor: Colors.black,
    useSafeArea: true,
    useRootNavigator: true,
    routeSettings: null,
  );
  // confettiController.dispose();
  return result;
}
