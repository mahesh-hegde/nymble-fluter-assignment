import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum PetType { dog, cat, rabbit, parrot }

class Pet {
  Pet(
      {required this.name,
      required this.type,
      required this.age,
      required this.price,
      required this.imageAssetName,
      this.adoptedOn});

  // For use with BLoC Cubit
  Pet.withAdoptionDate(Pet origin, DateTime adoptedOn)
      : this(
            name: origin.name,
            type: origin.type,
            age: origin.age,
            price: origin.price,
            imageAssetName: origin.imageAssetName,
            adoptedOn: adoptedOn);

  String name;
  PetType type;
  int age; // Age in months
  int price;
  String imageAssetName;
  DateTime? adoptedOn;

  bool get isAdopted => adoptedOn != null;
}

List<Pet> pets = [
  Pet(
      name: "Billy",
      type: PetType.cat,
      age: 48,
      price: 2100,
      imageAssetName: "cat-1.jpg"),
  Pet(
      name: "Dinga",
      type: PetType.dog,
      age: 56,
      price: 4000,
      imageAssetName: "dog-1.jpg"),
  Pet(
      name: "Pinto",
      type: PetType.dog,
      age: 20,
      price: 3000,
      imageAssetName: "dog-2.jpg"),
  Pet(
      name: "Ili",
      type: PetType.rabbit,
      age: 12,
      price: 1500,
      imageAssetName: "rabbit-1.jpg"),
  Pet(
      name: "Tippu",
      type: PetType.rabbit,
      age: 22,
      price: 2000,
      imageAssetName: "rabbit-2.jpg"),
  Pet(
      name: "Killi",
      type: PetType.parrot,
      age: 14,
      price: 2100,
      imageAssetName: "parrot-1.jpg"),
  Pet(
      name: "Golu",
      type: PetType.cat,
      age: 23,
      price: 5000,
      imageAssetName: "cat-2.jpg"),
  Pet(
      name: "John",
      type: PetType.dog,
      age: 30,
      price: 4250,
      imageAssetName: "dog-3.png"),
  Pet(
      name: "Jimmy",
      type: PetType.dog,
      age: 25,
      price: 3000,
      imageAssetName: "dog-4.jpg"),
  Pet(
      name: "Gundu",
      type: PetType.rabbit,
      age: 6,
      price: 1200,
      imageAssetName: "rabbit-3.jpg"),
  Pet(
      name: "Raju",
      type: PetType.dog,
      age: 28,
      price: 3400,
      imageAssetName: "dog-5.jpg"),
  Pet(
      name: "Don",
      type: PetType.cat,
      age: 20,
      price: 1000,
      imageAssetName: "cat-3.jpg"),
  Pet(
      name: "Billa 2",
      type: PetType.dog,
      age: 36,
      price: 4500,
      imageAssetName: "dog-6.jpg"),
  Pet(
      name: "Timmy",
      type: PetType.parrot,
      age: 22,
      price: 1800,
      imageAssetName: "parrot-2.jpg"),
  Pet(
      name: "Don 2",
      type: PetType.parrot,
      age: 50,
      price: 4000,
      imageAssetName: "parrot-3.jpg"),
];

late SharedPreferences _prefs;

class PetCubit extends Cubit<Pet> {
  int index;
  PetCubit(this.index) : super(pets[index]);
  void adopt() {
    if (pets[index].isAdopted) {
      throw "${pets[index].name} is already adopted!";
    }
    final now = DateTime.now();
    // Synchronize changes to source
    pets[index].adoptedOn = now;
    _prefs.setInt("adoptedOn/${pets[index].name}", now.millisecondsSinceEpoch);

    emit(Pet.withAdoptionDate(state, now));
  }
}

class Search {
  Search.of(this.query, this.types);
  Search() : this.of("", {});

  String query;
  Set<PetType> types;
}

class SearchCubit extends Cubit<Search> {
  SearchCubit() : super(searchQueryState);
  void setSearchTerm(String query) => emit(Search.of(query, state.types));

  void toggleType(PetType type) {
    final types = state.types;
    if (types.contains(type)) {
      types.remove(type);
    } else {
      types.add(type);
    }
    emit(Search.of(state.query, types));
  }
}

// Keep state global so that it persists when switching tabs, or re-rendering
// home screen.
final searchQueryState = Search.of("", PetType.values.toSet());

/// Filter pets list by search criteria and return a list of indices.
/// It doesn't return data, only indices - so that pets[] list is the single
/// source of information.
List<int> getPetIndices(Search search) {
  List<int> indices = [];
  for (int i = 0; i < pets.length; i++) {
    final pet = pets[i];
    final petName = pet.name.toLowerCase();
    final query = search.query.toLowerCase();
    if (search.query.isEmpty || petName.contains(query)) {
      if (search.types.contains(pet.type)) {
        indices.add(i);
      }
    }
  }
  return indices;
}

Future<void> initializeAdoptionStatus() async {
  _prefs = await SharedPreferences.getInstance();
  for (Pet pet in pets) {
    final adoptedOn = _prefs.getInt("adoptedOn/${pet.name}");
    if (adoptedOn != null) {
      pet.adoptedOn = DateTime.fromMillisecondsSinceEpoch(adoptedOn);
    }
  }
}

String formatDate(DateTime? date) {
  if (date == null) return 'N/A';
  const monthNames = [
    '???',
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  return "${date.day} ${monthNames[date.month]}";
}
