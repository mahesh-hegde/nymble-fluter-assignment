import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:pet_adoption_app_assignment/ui_util/stateful_checkbox.dart';
import 'package:pet_adoption_app_assignment/ui_util/confetti_dialog.dart';

import 'package:pet_adoption_app_assignment/data.dart';

const monospaceTextStyle = TextStyle(
  fontFamily: "Monospace",
  color: Colors.white,
);

// Helper widget to display pet image as Avatar
class Avatar extends StatelessWidget {
  final String assetName;
  const Avatar(this.assetName, {Key? key}) : super(key: key);
  static const assetPath = "assets/images/";

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: SizedBox(
        height: 100,
        width: 100,
        child: Image.asset(
          assetPath + assetName,
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initializeAdoptionStatus();
  runApp(const PetsApp());
}

class PetsApp extends StatelessWidget {
  const PetsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pet Adoption',
      // TODO: Add button to switch between dark mode and light mode
      theme: ThemeData.dark().copyWith(
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.black,
          elevation: 2.0,
        ),
        backgroundColor: Colors.black,
        buttonTheme: const ButtonThemeData(
          colorScheme: ColorScheme.highContrastDark(),
        ),
        primaryColor: Colors.teal,
      ),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(icon: Icon(Icons.pets_outlined), text: "Pets"),
                Tab(icon: Icon(Icons.history_outlined), text: "History"),
              ],
            ),
            title: const Text('Pet Adoption'),
          ),
          body: const TabBarView(
            children: [
              HomePage(),
              HistoryPage(),
            ],
          ),
        ),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SearchCubit(),
      child: Center(
        child: Column(children: const [
          SearchForm(),
          Expanded(child: PetsList()),
        ]),
      ),
    );
  }
}

class SearchForm extends StatelessWidget {
  const SearchForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final state = context.read<SearchCubit>().state;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
      child: Column(
        children: [
          TextFormField(
            // Use cubit only when re-rendering, no need to use blocbuilder.
            initialValue: state.query,
            onChanged: (query) =>
                context.read<SearchCubit>().setSearchTerm(query),
            decoration: const InputDecoration(
              prefixIcon: Icon(Icons.search),
              border: OutlineInputBorder(),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              for (final petType in PetType.values)
                StatefulCheckbox(
                  label: petType.name,
                  initialState: state.types.contains(petType),
                  onChange: (state) =>
                      context.read<SearchCubit>().toggleType(petType),
                )
            ],
          )
        ],
      ),
    );
  }
}

class PetsList extends StatelessWidget {
  const PetsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchCubit, Search>(
      builder: (context, search) {
        final petIndices = getPetIndices(search);
        return ListView.builder(
          shrinkWrap: true,
          itemCount: petIndices.length,
          itemBuilder: (context, index) {
            return BlocProvider(
              key: Key("${petIndices[index]}"),
              create: (_) => PetCubit(petIndices[index]),
              child: PetDetailsTile(key: Key("tile/${petIndices[index]}")),
            );
          },
        );
      },
    );
  }
}

class PetDetailsTile extends StatelessWidget {
  const PetDetailsTile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PetCubit, Pet>(
      builder: (context, pet) => ListTile(
        leading: Avatar(pet.imageAssetName),
        title: Text(
          pet.name,
          style: pet.isAdopted
              ? TextStyle(
                  color: Colors.grey.shade400,
                  decoration: TextDecoration.lineThrough,
                )
              : null,
        ),
        subtitle: Text(pet.type.name),
        trailing: pet.isAdopted
            ? const Text(
                "Adopted",
                style: TextStyle(color: Colors.blue),
              )
            : null,
        onTap: () {
          // Go to details page
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (iContext) => BlocProvider(
                // We cannot directly create a route here, since provider
                // will not be accessible in different route.
                // But we can't also create a new Cubit, because that will
                // create new state every time we navigate to this page.
                //
                // TODO: Find idiomatic way to pass cubits between routes.
                create: (_) => context.read<PetCubit>(),
                child: const PetDetailsPage(),
              ),
            ),
          );
        },
      ),
    );
  }
}

class PetDetailsPage extends StatelessWidget {
  // For widget test purpose; Due to some reason this page is not
  // rendering properly in widget test.
  final bool showImage;
  const PetDetailsPage({this.showImage = true, Key? key}) : super(key: key);

  Widget infoText(String info) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: Text(
          info,
          style: monospaceTextStyle,
          textAlign: TextAlign.justify,
        ),
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PetCubit, Pet>(
      builder: (context, pet) => Scaffold(
        appBar: AppBar(
          title: Text(pet.name),
        ),
        body: Hero(
          tag: 'hero/${pet.name}',
          child: ListView(
            children: [
              if (showImage)
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InteractiveViewer(
                    boundaryMargin: const EdgeInsets.all(0.0),
                    minScale: 1.0,
                    maxScale: 4.0,
                    child: AspectRatio(
                      aspectRatio: 1,
                      child: Image.asset(
                        "assets/images/${pet.imageAssetName}",
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                  ),
                ),
              infoText("Name: ${pet.name}"),
              infoText("Age: ${pet.age} months"),
              infoText("Price: ₹${pet.price}"),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Center(
                  child: OutlinedButton(
                    onPressed: pet.isAdopted
                        ? null
                        : () {
                            context.read<PetCubit>().adopt();
                            showConfettiDialog(
                              context: context,
                              text: "You have adopted ${pet.name}!",
                            );
                          },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        pet.isAdopted ? "ADOPTED" : "ADOPT ME (₹${pet.price})",
                        style: const TextStyle(fontSize: 18.0),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class HistoryTile extends StatelessWidget {
  final Pet pet;
  const HistoryTile({required this.pet, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Hero(
        tag: 'hero/${pet.name}',
        child: ListTile(
          leading: Avatar(pet.imageAssetName),
          title: Text(pet.name),
          subtitle: Text(pet.type.name),
          trailing: Text(formatDate(pet.adoptedOn)),
        ),
      );
}

class HistoryPage extends StatelessWidget {
  const HistoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final history = pets.where((pet) => pet.isAdopted).toList();
    // sort in reverse order
    history.sort((a, b) => b.adoptedOn!.compareTo(a.adoptedOn!));
    return Center(
      child: ListView.builder(
        itemCount: history.length,
        itemBuilder: (context, i) =>
            HistoryTile(pet: history[history.length - 1 - i]),
      ),
    );
  }
}
